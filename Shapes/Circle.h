#pragma once

#include "Shape.h"
#include "Point.h"

#define PI 3.14

class Circle : public Shape
{
private:
	Point _center;
	double _radius;
public:
	Circle(const Point& center, double radius, const std::string& name);
	virtual ~Circle();
	double getArea() const override;
	double getPerimeter() const override;
	const Point& getCenter() const;
	double getRadius() const;
	void move(const Point& other) override;

	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas) override;

	// override functions if need (virtual + pure virtual)

};