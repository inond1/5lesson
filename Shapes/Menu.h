#pragma once
#include "Shape.h"
#include "Canvas.h"
#include "Arrow.h"
#include "Circle.h"
#include "Triangle.h"
#include "Rectangle.h"
#include <vector>

enum Option {
	ADD_SHAPE = 0,
	MODIFY_OR_INFO,
	DELETE_ALL,
	EXIT, 
	CIRCLE = 0,
	ARROW,
	TRIANGLE,
	RECTANGLE,
	MOVE = 0,
	DETAILS,
	REMOVE_SHAPE
};

class Menu
{
public:

	Menu();
	~Menu();
	
	void mainMenu();
	
	
	// more functions..

private:
	Canvas _canvas;
	std::vector<Shape*> _shapes;
	void ModifyOrInfo();
	void addShape();
	void addArrow();
	void addCircle();
	void addTriangle();
	void addRectangle();
	void input(int*);
	void input(double*);
	void deleteAll();
	void clear();
	void draw();
	void move(size_t pos);
};
