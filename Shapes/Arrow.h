#pragma once
#include "Polygon.h"

class Arrow : public Shape
{
public:
	Arrow(const Point& a, const Point& b, const std::string& name);
	
	virtual ~Arrow();

	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas);
	double getArea() const override;
	double getPerimeter() const override;
	void move(const Point& other) override;
	// override functions if need (virtual + pure virtual)

private:
	std::vector<Point> _points;
};