#pragma once
#include "Polygon.h"


namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	public:
		void draw(const Canvas& canvas) override;
		void clearDraw(const Canvas& canvas) override;
		// There's a need only for the top left corner 
		Rectangle(const Point& a, double length, double width, const std::string& name);
		virtual ~Rectangle();
		double getArea() const override;
		double getPerimeter() const override;
		// override functions if need (virtual + pure virtual)

	};
}