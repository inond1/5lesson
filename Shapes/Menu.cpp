#include "Menu.h"

Menu::Menu()
{
	this->_canvas;
	this->_shapes;
}

Menu::~Menu()
{
	deleteAll();
}

void Menu::mainMenu()
{
	int choice = 0;
	while (choice != EXIT)
	{
		std::cout << "Enter 0 to add a new shape.\n";
		std::cout << "Enter 1 to modify or get information from a current shape.\n";
		std::cout << "Enter 2 to delete all of the shapes.\n";
		std::cout << "Enter 3 to exit.\n";
		input(&choice);
		system("CLS");
		switch (choice)
		{
		case ADD_SHAPE:
			
			addShape();
			break;
		case MODIFY_OR_INFO:
			if (!this->_shapes.empty())
			{
				ModifyOrInfo();
			}
			break;
		case DELETE_ALL:
			deleteAll();
			break;
		case EXIT:
			break;
		default:
			std::cout << "[ERROR] wrong Input!\n";
			break;
		}
		system("CLS");
	}

}

void Menu::ModifyOrInfo()
{
	int choiceShape = -1;
	int choice = -1;
	for (size_t i = 0; i < this->_shapes.size(); i++)
	{
		std::cout << "Enter " << i << " for " << this->_shapes[i]->getName() << "(" << this->_shapes[i]->getType() << ")\n";
	}
	while (choiceShape < 0 || (size_t)choiceShape >= this->_shapes.size())
	{
		input(&choiceShape);
	} 
	while (choice< MOVE || choice>REMOVE_SHAPE)
	{
		std::cout << "Enter 0 to move the shape.\n";
		std::cout << "Enter 1 to get its details.\n";
		std::cout << "Enter 2 to remove the shape.\n";
		input(&choice);
		system("CLS");
		switch (choice)
		{
		case MOVE:
			move(choiceShape);
			break;
		case DETAILS:
			
			this->_shapes[choiceShape]->printDetails();
			system("pause");
			break;
		case REMOVE_SHAPE:
			(*(choiceShape + this->_shapes.begin()))->clearDraw(this->_canvas);
			this->_shapes.erase(choiceShape + this->_shapes.begin());
			system("CLS");
			break;
		default:
			system("CLS");
			std::cout << "wrong input!\n";
		}
		system("CLS");
	}
	
}

void Menu::addShape()
{
	int choice = -1;
	while (choice< CIRCLE || choice > RECTANGLE)
	{
		std::cout << "Enter 0 to add a circle.\n";
		std::cout << "Enter 1 to add an arrow.\n";
		std::cout << "Enter 2 to add a triangle.\n";
		std::cout << "Enter 3 to add a rectangle.\n";
		input(&choice);
		system("CLS");
		switch (choice)
		{
		case CIRCLE:
			addCircle();
			break;
		case ARROW:
			addArrow();
			break;
		case TRIANGLE:
			addTriangle();
			break;
		case RECTANGLE:
			addRectangle();
			break;
		default:
			std::cout << "wrong Input!\n";
			break;
		}
		system("CLS");
	}
}

void Menu::addArrow()
{
	double x = 0, y = 0;
	std::string name;
	std::cout << "enter X,Y for first point:\n";
	std::cout << "x:\t";
	input(&x);
	std::cout << "y:\t";
	input(&y);
	Point firstPoint = Point(x, y);
	std::cout << "enter X,Y for second point:\n";
	std::cout << "x:\t";
	input(&x);
	std::cout << "y:\t";
	input(&y);
	Point secondPoint = Point(x, y);
	std::cout << "enter name of shape:\t";
	std::cin >> name;
	this->_shapes.push_back(new Arrow(firstPoint, secondPoint, name));
	this->draw();
	//this->_shapes.back()->draw(this->_canvas);
	system("CLS");
}

void Menu::addCircle()
{
		double x = 0, y = 0, radius = 0;
		std::string name;

		std::cout << "Enter X:\t";
		input(&x);
		std::cout << "Enter Y:\t";
		input(&y);
		std::cout << "Enter radius:\t";
		input(&radius);
		std::cout << "Enter name:\t";
		std::cin >> name;
	try{
		this->_shapes.push_back(new Circle(Point(x, y), radius, name));
		//this->_shapes.back()->draw(this->_canvas);
		this->draw();
		system("CLS");
	}
	catch (...)
	{
		system("CLS");
		std::cout << "[ERROR] wrong input! try again...\n";
		addCircle();
	}
}

void Menu::addTriangle()
{
	double x = 0, y = 0;
	std::string name;
	std::cout << "Enter X of first point:\t";
	input(&x);
	std::cout << "Enter Y of first point:\t";
	input(&y);
	Point first(x, y);
	std::cout << "Enter X of second point:\t";
	input(&x);
	std::cout << "Enter Y of second point:\t";
	input(&y);
	Point second(x, y);
	std::cout << "Enter X of third point:\t";
	input(&x);
	std::cout << "Enter Y of third point:\t";
	input(&y);
	std::cout << "Enter name:\t";
	std::cin >> name;
	try {
		this->_shapes.push_back(new Triangle(first, second, Point(x, y), name));
		//this->_shapes.back()->draw(this->_canvas);
		this->draw();
		system("CLS");
	}
	catch (...)
	{
		system("CLS");
		std::cout << "[ERROR] wrong input! try again...\n";
		addTriangle();
	}
}

void Menu::addRectangle()
{
	double x = 0, y = 0;
	double length = 0, width = 0;
	std::string name;
	std::cout << "Enter X of top left corner:\t";
	input(&x);
	std::cout << "Enter Y of top left corner:\t";
	input(&y);
	std::cout << "Enter length:\t";
	input(&length);
	std::cout << "Enter width:\t";
	input(&width);
	std::cout << "Enter name:\t";
	std::cin >> name;
	try {
		this->_shapes.push_back(new myShapes::Rectangle(Point(x, y), length, width, name));
		//this->_shapes.back()->draw(this->_canvas);
		this->draw();
		system("CLS");
	}
	catch (...)
	{
		system("CLS");
		std::cout << "[ERROR] wrong input! try again...\n";
		addRectangle();
	}
}

void Menu::input(int* var)
{
	while (!(std::cin >> *var))
	{
		std::cin.clear();
		std::string line;
		std::getline(std::cin, line);
		std::cout << "I am sorry, but '" << line << "' is not valid\n";
	}
}

void Menu::input(double* var)
{
	while (!(std::cin >> *var))
	{
		std::cin.clear();
		std::string line;
		std::getline(std::cin, line);
		std::cout << "I am sorry, but '" << line << "' is not valid\n";
	}
}

void Menu::deleteAll()
{
	while (!this->_shapes.empty()) {
		this->_shapes.back()->clearDraw(this->_canvas);
		delete this->_shapes.back();
		this->_shapes.pop_back();
	}
}

void Menu::clear()
{
	for (auto it = this->_shapes.begin(); it != this->_shapes.end(); it++)
	{
		(*it)->clearDraw(this->_canvas);

	}
}

void Menu::draw()
{
	clear();
	for (auto it = this->_shapes.begin(); it != this->_shapes.end(); it++)
	{

		(*it)->draw(this->_canvas);

	}
}

void Menu::move(size_t pos)
{
	double x = 0, y = 0;
	std::cout << "Enter X of the moving scale:\t";
	input(&x);
	std::cout << "Enter Y of the moving scale:\t";
	input(&y);
	this->clear();
	this->_shapes[pos]->move(Point(x, y));
	this->draw();
	
}

