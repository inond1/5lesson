#include "Circle.h"

Circle::Circle(const Point& center, double radius, const std::string& name):Shape(name, "Circle")
{
	if (radius > 0) {
		this->_center = center;
		this->_radius = radius;
	}
	else
	{
		std::cerr << "[ERROR] radius is negative\n";
		throw std::exception();
	}
}

Circle::~Circle()
{
}

double Circle::getArea() const
{
	return pow(this->_radius,2)* acos(-1.0);
}

double Circle::getPerimeter() const
{
	return 2*acos(-1.0)*this->_radius;
}

const Point& Circle::getCenter() const
{
	return this->_center;
}

double Circle::getRadius() const
{
	return this->_radius;
}

void Circle::move(const Point& other)
{
	this->_center += other;
}

void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(this->_center, this->_radius);
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(this->_center, this->_radius);
}
