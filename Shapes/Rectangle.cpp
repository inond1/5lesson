#include "Rectangle.h"



void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& name):Polygon("Rectangle", name)
{
	if (length <= 0 || width <= 0)
	{
		std::cerr << "[ERROR] width or length is negative\n";
		throw std::exception();
	}
	else
	{
		this->_points.push_back(a);
		this->_points.push_back(a+Point(length, width));
	}
}

myShapes::Rectangle::~Rectangle()
{
}

double myShapes::Rectangle::getArea() const
{
	return (this->_points[1].getY()-this->_points[0].getY())*(this->_points[1].getX()-this->_points[0].getX());
}

double myShapes::Rectangle::getPerimeter() const
{
	return 2*(this->_points[1].getY() - this->_points[0].getY()+ this->_points[1].getX() - this->_points[0].getX());
}
