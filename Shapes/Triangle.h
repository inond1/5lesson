#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
public:
	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas) override;
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& name);
	virtual ~Triangle();
	double getArea() const override;
	double getPerimeter() const override;
	// override functions if need (virtual + pure virtual)
};

bool isSameIncline(const Point& a, const Point& b, const Point& c);