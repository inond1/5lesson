#pragma once
#include <cmath> 
class Point
{
private:
	double _x;
	double _y;
public:
	Point(double x, double y);
	Point(const Point& other);
	Point();
	virtual ~Point();

	Point operator+(const Point& other) const;
	Point& operator+=(const Point& other);
	double distance(const Point& other);
	double getX() const;
	double getY() const;

	double distance(const Point& other) const;
};