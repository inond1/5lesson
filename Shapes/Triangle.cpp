#include "Triangle.h"



void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& name):Polygon("Triangle", name)
{
	if((a.getX() == b.getX() && b.getX() == c.getX()) || (a.getY() == b.getY() && b.getX() == c.getY())|| isSameIncline(a, b, c))
	{
		std::cerr << "[ERROR] points are on the same line\n";
		throw std::exception();
	}
	else
	{
		this->_points.push_back(a);
		this->_points.push_back(b);
		this->_points.push_back(c);
	}
}

Triangle::~Triangle()
{
}

double Triangle::getArea() const
{ //heron formula
	return sqrt((this->getPerimeter()/2)*(this->getPerimeter()/2-this->_points[0].distance(this->_points[1]))*
		(this->getPerimeter() / 2-this->_points[1].distance(this->_points[2]))*
		(this->getPerimeter() / 2-this->_points[2].distance(this->_points[0])));
}

double Triangle::getPerimeter() const
{
	return this->_points[0].distance(this->_points[1])+this->_points[1].distance(this->_points[2]) +this->_points[2].distance(this->_points[0]);
}

bool isSameIncline(const Point& a, const Point& b, const Point& c)
{
	return (a.getY() - b.getY()) / (a.getX() - b.getX()) == (a.getY() - c.getY()) / (a.getX() - c.getX());
}
