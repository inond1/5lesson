#include "Polygon.h"

Polygon::Polygon(const std::string& type="", const std::string& name=""):Shape(name, type)
{}

Polygon::Polygon()
{}


void Polygon::move(const Point& other)
{
	for (size_t i = 0; i < this->_points.size(); i++)
	{
		this->_points[i] += other;
	}
}
